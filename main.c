#include <stdio.h>
#include <espressif/esp_wifi.h>
#include <espressif/esp_sta.h>
#include <esp/uart.h>
#include <esp8266.h>
#include <FreeRTOS.h>
#include <task.h>

#include <homekit/homekit.h>
#include <homekit/characteristics.h>
#include "wifi.h"

#include <softuart/softuart.h>
#include <ultrasonic/ultrasonic.h>

/*
D0 16;
D1 5;
D2 4;
D3 0;
D4 2;
D5 14;
D6 12;
D7 13;
D8 15;
RX 3;
TX 1;
*/

void soft_serial_init()
{
    softuart_open(0, 9600, /*rx D2*/ 4, /*tx D1*/ 5);
}

static void wifi_init() {
    struct sdk_station_config wifi_config = {
        .ssid = WIFI_SSID,
        .password = WIFI_PASSWORD,
    };

    sdk_wifi_set_opmode(STATION_MODE);
    sdk_wifi_station_set_config(&wifi_config);
    sdk_wifi_station_connect();
}


#define DIGITAL_OUT_SIZE 1 /*5*/

const int digital_out_pins[DIGITAL_OUT_SIZE] = { /*16, 5, 4, 0,*/ 2 };
volatile bool digital_out_values[DIGITAL_OUT_SIZE] = { /*false, false, false, false,*/ false };

void digital_update(int port) {
    // Потому что у релешек обратнвя логика
    gpio_write(digital_out_pins[port], digital_out_values[port] ? 1 : 0);
}

void digital_init() {
  for (int port = 0; port < DIGITAL_OUT_SIZE; ++port)
  {
    gpio_enable(digital_out_pins[port], GPIO_OUTPUT);
    digital_update(port);
  }
}

void digital_identify_task(void *_args) {
    // i tak soidet
    const int digital_out = (int)(_args);

    for (int i=0; i<3; i++) {
        for (int j=0; j<2; j++) {
            digital_out_values[digital_out] = true;
            digital_update(digital_out);
            vTaskDelay(100 / portTICK_PERIOD_MS);
            digital_out_values[digital_out] = false;
            digital_update(digital_out);
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }

        vTaskDelay(250 / portTICK_PERIOD_MS);
    }

    digital_out_values[digital_out] = false;
    digital_update(digital_out);

    vTaskDelete(NULL);
}

void digital_blink_task(void *_args) {
    // zazharitsa
    const int digital_out = (int)(_args);

    while (true) {
            digital_out_values[digital_out] = !digital_out_values[digital_out];
            digital_update(digital_out);
            vTaskDelay(10000 / portTICK_PERIOD_MS);
    }

    digital_out_values[digital_out] = false;
    digital_update(digital_out);

    vTaskDelete(NULL);
}

void digital_blink_init(int port)
{
  xTaskCreate(digital_blink_task, "Digital blink", 128, (void*)port, 2, NULL);
}

#define digital_identify_x(port) \
void digital_identify_##port(homekit_value_t _value) { \
    printf("Digital ##port identify\n"); \
    xTaskCreate(digital_identify_task, "Digital identify ##port", 128, (void*)port, 2, NULL); \
}

#define digital_out_get_x(port) \
homekit_value_t digital_out_get_##port() { \
    return HOMEKIT_BOOL(digital_out_values[port]); \
}

#define digital_out_set_x(port) \
void digital_out_set_##port(homekit_value_t value) { \
    if (value.format != homekit_format_bool) { \
        printf("Invalid value format for port ##port: %d\n", value.format); \
        return; \
    } \
    digital_out_values[port] = value.bool_value; \
    digital_update(port); \
}

#define digital_out_get_set_x(port) \
digital_out_get_x(port) \
digital_out_set_x(port)

// expand macros to functions

digital_identify_x(0/*4*/)
//digital_out_get_set_x(1)
//digital_out_get_set_x(2)
//digital_out_get_set_x(3)
digital_out_get_set_x(0/*4*/)

/*
===================================================================
DIMMER
*/

volatile bool dimmer_on = true;
volatile int dimmer_brightness = 24;

void dimmer_refresh()
{
  char send[0];

  if (dimmer_on)
  {
    if (dimmer_brightness <= 0)
    {
      send[0] = 255;
    }
    else if (dimmer_brightness >= 100)
    {
      send[0] = 0;
    }
    else
    {
      send[0] = 255 - dimmer_brightness * 255 / 100;
    }
  }
  else
  {
    send[0] = 255;
  }

  //char buf[10] = { 0 };
  //sprintf(buf, "%d\n", send);
  //softuart_puts(0, buf);
  softuart_puts(0, send);

  printf(">>> HOF: DIMMER Actual sent [%s]\n", send);
}

homekit_value_t dimmer_on_get() {
    printf(">>> HOF: DIMMER ON GET\n");
    return HOMEKIT_BOOL(dimmer_on);
}

void dimmer_on_set(homekit_value_t value) {
    printf(">>> HOF: DIMMER ON SET\n");

    if (value.format != homekit_format_bool) {
        printf(">>> HOF: Invalid value format: %d\n", value.format);
        return;
    }

    dimmer_on = value.bool_value;
    if (dimmer_on) {
        printf(">>> HOF: DIMMER ON SET: TRUE\n");
    } else {
        printf(">>> HOF: DIMMER ON SET: FALSE\n");
    }

    dimmer_refresh();
}

homekit_value_t dimmer_brightness_get() {
    printf(">>> HOF: DIMMER BRIGHTNESS GET\n");
    return HOMEKIT_INT(dimmer_brightness);
}

void dimmer_brightness_set(homekit_value_t value) {
    printf(">>> HOF: DIMMER BRIGHTNESS SET\n");
    if (value.format != homekit_format_int) {
        printf("Invalid value format: %d\n", value.format);
        return;
    }

    dimmer_brightness = value.int_value;
    printf(">>> HOF: DIMMER BRIGHTNESS SET: %d\n", dimmer_brightness);

    dimmer_refresh();
}

/*
===================================================================
*/


/*
===================================================================
MOODLIGHT
*/

volatile bool moodlight_on = false;
volatile int moodlight_brightness = 50;
volatile float moodlight_hue = 50;

homekit_value_t moodlight_on_get() {
    printf(">>> HOF: MOODLIGHT ON GET\n");
    return HOMEKIT_BOOL(moodlight_on);
}

void moodlight_on_set(homekit_value_t value) {
    printf(">>> HOF: MOODLIGHT ON SET\n");

    if (value.format != homekit_format_bool) {
        printf(">>> HOF: Invalid value format: %d\n", value.format);
        return;
    }

    moodlight_on = value.bool_value;
    if (moodlight_on) {
        printf(">>> HOF: MOODLIGHT ON SET: TRUE\n");
    } else {
        printf(">>> HOF: MOODLIGHT ON SET: FALSE\n");
    }
}

homekit_value_t moodlight_brightness_get() {
    printf(">>> HOF: MOODLIGHT BRIGHTNESS GET\n");
    return HOMEKIT_INT(moodlight_brightness);
}

void moodlight_brightness_set(homekit_value_t value) {
    printf(">>> HOF: MOODLIGHT BRIGHTNESS SET\n");
    if (value.format != homekit_format_int) {
        printf("Invalid value format: %d\n", value.format);
        return;
    }

    moodlight_brightness = value.int_value;
    printf(">>> HOF: MOODLIGHT BRIGHTNESS SET: %d\n", moodlight_brightness);
}

homekit_value_t moodlight_hue_get() {
    printf(">>> HOF: MOODLIGHT HUE GET\n");
    return HOMEKIT_FLOAT(moodlight_hue);
}

void moodlight_hue_set(homekit_value_t value) {
    printf(">>> HOF: MOODLIGHT HUE SET\n");
    if (value.format != homekit_format_float) {
        printf("Invalid value format: %d\n", value.format);
        return;
    }

    moodlight_hue = value.float_value;
    printf(">>> HOF: MOODLIGHT HUE SET: %.6f\n", moodlight_hue);
}

/*
===================================================================
*/


/*
===================================================================
MOTOIN SENSOR
*/

const int motionTrigPin = 0;
const int motionEchoPin = 14;

const int max_distance_cm = 500; // 5m max

/*void delay_ms(uint32_t ms)
{
    for (uint32_t i = 0; i < ms; i ++)
        sdk_os_delay_us(1000);
}*/

#include <lwip/sockets.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <espressif/esp_common.h>
#include <unistd.h>

#include "espressif/esp_common.h"
#include "esp/uart.h"

#include <unistd.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"


#define WEB_SERVER "192.168.2.68"
#define WEB_PORT "8080"
#define WEB_PATH "/alert?serial_number=35651654321684351"

bool motion_detected = false;


void http_get_task(void *pvParameters)
{
    int successes = 0, failures = 0;
    printf("HTTP get task starting...\r\n");

    while(1) {

        if (!motion_detected) {
            continue;
        }

        const struct addrinfo hints = {
            .ai_family = AF_UNSPEC,
            .ai_socktype = SOCK_STREAM,
        };
        struct addrinfo *res;

        printf("Running DNS lookup for %s...\r\n", WEB_SERVER);
        int err = getaddrinfo(WEB_SERVER, WEB_PORT, &hints, &res);

        if (err != 0 || res == NULL) {
            printf("DNS lookup failed err=%d res=%p\r\n", err, res);
            if(res)
                freeaddrinfo(res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }


        struct sockaddr *sa = res->ai_addr;
        if (sa->sa_family == AF_INET) {
            printf("DNS lookup succeeded. IP=%s\r\n", inet_ntoa(((struct sockaddr_in *)sa)->sin_addr));
        }

        int s = socket(res->ai_family, res->ai_socktype, 0);
        if(s < 0) {
            printf("... Failed to allocate socket.\r\n");
            freeaddrinfo(res);
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }

        printf("... allocated socket\r\n");

        if(connect(s, res->ai_addr, res->ai_addrlen) != 0) {
            close(s);
            freeaddrinfo(res);
            printf("... socket connect failed.\r\n");
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }

        printf("... connected\r\n");
        freeaddrinfo(res);

        const char *req =
            "GET "WEB_PATH" HTTP/1.1\r\n"
            "Host: "WEB_SERVER"\r\n"
            "User-Agent: esp-open-rtos/0.1 esp8266\r\n"
            "Connection: close\r\n"
            "\r\n";
        if (write(s, req, strlen(req)) < 0) {
            printf("... socket send failed\r\n");
            close(s);
            vTaskDelay(4000 / portTICK_PERIOD_MS);
            failures++;
            continue;
        }
        printf("... socket send success\r\n");

        static char recv_buf[128];
        int r;
        do {
            bzero(recv_buf, 128);
            r = read(s, recv_buf, 127);
            if(r > 0) {
                printf("%s", recv_buf);
            }
        } while(r > 0);

        printf("... done reading from socket. Last read return=%d errno=%d\r\n", r, errno);
        if(r != 0)
            failures++;
        else
            successes++;
        close(s);
        printf("successes = %d failures = %d\r\n", successes, failures);
        // for(int countdown = 10; countdown >= 0; countdown--) {
        //     printf("%d... ", countdown);
        //     vTaskDelay(1000 / portTICK_PERIOD_MS);
        // }
        printf("\r\nStarting again!\r\n");
    }
}

void motion_detected_callback(homekit_characteristic_t *_ch, homekit_value_t on, void *context)
{

    printf(">>> HoF: CALLBACK motion detected\n");

    motion_detected = on.bool_value;
}




homekit_characteristic_t motion_detector = HOMEKIT_CHARACTERISTIC_(
    MOTION_DETECTED, false, .callback=HOMEKIT_CHARACTERISTIC_CALLBACK(motion_detected_callback)
);

homekit_characteristic_t motion_sensor_status_active = HOMEKIT_CHARACTERISTIC_(STATUS_ACTIVE, false);

void motion_sensor_task(void *_args) {
    ultrasonic_sensor_t sensor = {
        .trigger_pin = motionTrigPin,
        .echo_pin = motionEchoPin
    };

    ultrasoinc_init(&sensor);

    while (true)
    {
      const int32_t distance = ultrasoinc_measure_cm(&sensor, max_distance_cm);
      if (distance < 0)
      {
        switch (distance)
        {
        case ULTRASONIC_ERROR_PING:
          printf("Cannot ping (device is in invalid state)\n");
        break;
        case ULTRASONIC_ERROR_PING_TIMEOUT:
          printf("Ping timeout (no device found)\n");
        break;
        case ULTRASONIC_ERROR_ECHO_TIMEOUT:
          printf("Echo timeout (i.e. distance too big)\n");
        break;
        }

        motion_detector.value.bool_value = false;
        motion_sensor_status_active.value.bool_value = false;

        homekit_characteristic_notify(&motion_detector, HOMEKIT_BOOL(false));
        homekit_characteristic_notify(&motion_sensor_status_active, HOMEKIT_BOOL(false));
      }
      else
      {
        const bool detected = distance < 10;
        if (detected)
        {
          printf("Distance: %d cm, [DETECTED]\n", distance);
        }

        motion_detector.value.bool_value = true;
        motion_sensor_status_active.value.bool_value = true;

        homekit_characteristic_notify(&motion_detector, HOMEKIT_BOOL(true));
        homekit_characteristic_notify(&motion_sensor_status_active, HOMEKIT_BOOL(true));
      }

       vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void motion_sensor_init() {
    xTaskCreate(motion_sensor_task, "Motion Sensor", 256, NULL, 1, NULL);

    xTaskCreate(http_get_task, "EmptyTask", 2046, NULL, 0, NULL);
}

/*
===================================================================
*/

homekit_accessory_t *accessories[] = {
    HOMEKIT_ACCESSORY(.id=1, .category=homekit_accessory_category_lightbulb, .services=(homekit_service_t*[]){
        HOMEKIT_SERVICE(ACCESSORY_INFORMATION, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Hackaton-2018 LIGHTBULB"),
            HOMEKIT_CHARACTERISTIC(MANUFACTURER, "HoF"),
            HOMEKIT_CHARACTERISTIC(SERIAL_NUMBER, "000000000001"),
            HOMEKIT_CHARACTERISTIC(MODEL, "DevBoard"),
            HOMEKIT_CHARACTERISTIC(FIRMWARE_REVISION, "0.0"),
            HOMEKIT_CHARACTERISTIC(IDENTIFY, digital_identify_0/*4*/),
            NULL
        }),
        HOMEKIT_SERVICE(LIGHTBULB, .primary=true, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Bulb 1 On/Off"),
            HOMEKIT_CHARACTERISTIC(
                ON, false,
                .getter=digital_out_get_0/*4*/,
                .setter=digital_out_set_0/*4*/
            ),
            NULL
        }),
        HOMEKIT_SERVICE(LIGHTBULB, .primary=false, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Dimmer On/Off/Brightness"),
            HOMEKIT_CHARACTERISTIC(
                ON, true,
                .getter=dimmer_on_get,
                .setter=dimmer_on_set
            ),
            HOMEKIT_CHARACTERISTIC(
                BRIGHTNESS, 24,
                .getter=dimmer_brightness_get,
                .setter=dimmer_brightness_set
            ),
            NULL
        }),
        HOMEKIT_SERVICE(LIGHTBULB, .primary=false, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Moodlight On/Off/Brightness/Hue"),
            HOMEKIT_CHARACTERISTIC(
                ON, false,
                .getter= moodlight_on_get,
                .setter= moodlight_on_set
            ),
            HOMEKIT_CHARACTERISTIC(
                BRIGHTNESS, 50,
                .getter=moodlight_brightness_get,
                .setter=moodlight_brightness_set
            ),
            HOMEKIT_CHARACTERISTIC(
                HUE, 50,
                .getter=moodlight_hue_get,
                .setter=moodlight_hue_set
            ),
            NULL
        }),
        NULL
    }),
    HOMEKIT_ACCESSORY(.id=2, .category=homekit_accessory_category_sensor, .services=(homekit_service_t*[]){
        HOMEKIT_SERVICE(ACCESSORY_INFORMATION, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Hackaton-2018 SENSOR"),
            HOMEKIT_CHARACTERISTIC(MANUFACTURER, "HoF"),
            HOMEKIT_CHARACTERISTIC(SERIAL_NUMBER, "000000000001"),
            HOMEKIT_CHARACTERISTIC(MODEL, "DevBoard"),
            HOMEKIT_CHARACTERISTIC(FIRMWARE_REVISION, "0.0"),
            HOMEKIT_CHARACTERISTIC(IDENTIFY, digital_identify_0/*4*/),
            NULL
        }),
        HOMEKIT_SERVICE(MOTION_SENSOR, .primary=true, .characteristics=(homekit_characteristic_t*[]){
            HOMEKIT_CHARACTERISTIC(NAME, "Motion Sensor"),
            &motion_detector, // READ ONLY
            &motion_sensor_status_active, // READ ONLY
            NULL
        }),
        NULL
    }),
    NULL
};

homekit_server_config_t config = {
    .accessories = accessories,
    .password = "111-11-111"
};

void user_init(void) {
    // set default usb-uart settings
    uart_set_baud(0, 115200);

    // connect to wifi
    wifi_init();

    // for test on boards with no WiFi
    //digital_blink_init(0/*1*/);

    // for lightbulb, simple output
    digital_init();

    // for dimmer, configure Adrduino Nano through uart
    soft_serial_init();

    // TODO initialization for moodlight

    // Motion Sensor
    motion_sensor_init();

    homekit_server_init(&config);
}
